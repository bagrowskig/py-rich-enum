from setuptools import setup, find_packages

setup(
    name='py-rich-enum',
    version='0.1',
    author='GB & MK',
    packages=find_packages(),
    include_package_data=True,
    url='https://bitbucket.org/bagrowskig/py-rich-enum',
    download_url='https://bitbucket.org/bagrowskig/py-rich-enum/downloads',
    install_requires=[
    ],
    classifiers=[
        'Programming Language :: Python',
    ],
)
