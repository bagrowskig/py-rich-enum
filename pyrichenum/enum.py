# -*- coding: utf8 -*-

from __future__ import unicode_literals

from collections import OrderedDict, Counter


class EnumMetaclass(type):
    """Metaclass for a rich enum.

    Each varible of type included in EnumMetaClass.VAR_TYPES with all capital
    name not starting from '__' is treated as an enum variable.

    Altered class gains: 'names', 'values' and 'choices' attributes.

    Example:

    class SomeEnum:

        VAR1 = 1
        VAR2 = 2
        LONG_VAR3 = 3

    becomes:

    class SomeEnum:

        __metaclass__ = EnumMetaclass

        VAR1 = 1
        VAR2 = 2
        LONG_VAR3 = 3


        names = {1: 'VAR1', 2: 'VAR2', 3: 'LONG_VAR3',}
        display_names = {1: 'Var1', 2: 'Var2', 3: 'Long var3',}
        values = [1, 2, 3]
        value_for_name = {'VAR1': 1, 'VAR2': 2, 'LONG_VAR3': 3}
        choices = [(1, 'VAR1'), (2, 'VAR2'), (3, 'LONG_VAR3')]
        display_choices = [(1, 'Var1'), (2, 'Var2'), (3, 'Long_var3')]
        as_json = { contains all the above lists/dictionaries and enum values }

    """
    EXTRA_FIELDS = [
        "values",
        "names",
        "value_for_name",
        "display_names",
        "choices",
        "display_choices",
        "as_json",
    ]

    VALUE_TYPES = [basestring, int, long, float]

    # control attributes
    SORT_ENUM_VALUES_ATTR_NAME = 'sort_enum_values'
    SORT_ENUM_VALUES_DEFAULT = True
    SORTED_KWARGS_ATTRIBUTE = 'enum_values_sorted_kwargs'

    ENUM_ATTRIBUTES_ATTR_NAME = 'enum_attributes'
    ENUM_EXCLUDED_ATTR_NAME = 'enum_excluded'
    PRIVATE_IS_ORDERED_ATTR_NAME = '_ordered_enum'

    @classmethod
    def make_display_name(cls, name):
        return name.replace('_', ' ').capitalize()

    @classmethod
    def is_valid_enum_name(cls, name):
        return name.isupper() and not name.startswith('__')

    @classmethod
    def is_valid_enum_value(cls, val):
        return any(map(lambda x: isinstance(val, x), cls.VALUE_TYPES))

    @classmethod
    def is_enum_attribute(cls, name, val):
        return cls.is_valid_enum_name(name) and cls.is_valid_enum_value(val)

    @classmethod
    def should_preserve_value_order(cls, bases, attrs):
        def is_ordered(cls):
            return (
                getattr(cls, '__metaclass__', None) == type(cls) and
                (
                    getattr(cls, cls.PRIVATE_IS_ORDERED_ATTR_NAME) or
                    getattr(cls, cls.SORT_ENUM_VALUES_ATTR_NAME)
                )
            )

        return (
            cls.should_sort_enum_values(attrs) or
            len(attrs.get(cls.ENUM_ATTRIBUTES_ATTR_NAME) or []) > 0 or
            any(map(is_ordered, bases))
        )

    @classmethod
    def should_sort_enum_values(cls, attrs):
        return attrs.get(
            cls.SORT_ENUM_VALUES_ATTR_NAME,
            cls.SORT_ENUM_VALUES_DEFAULT
        )

    @classmethod
    def sort_enum_attrs(cls, values, attrs):
        kwargs = attrs.get(cls.SORTED_KWARGS_ATTRIBUTE, {})
        kwargs.setdefault('key', lambda t: t[1])
        return OrderedDict(sorted(values.items(), **kwargs))

    @classmethod
    def get_enum_attrs(cls, bases, attrs, is_ordered):
        enum_attrs = cls.get_dict_class(is_ordered)()

        excluded = attrs.get(cls.ENUM_EXCLUDED_ATTR_NAME, [])
        enum_attributes = attrs.get(cls.ENUM_ATTRIBUTES_ATTR_NAME)
        enum_names = enum_attributes or attrs.keys()

        for name in enum_names:
            if name in excluded:
                if enum_attributes and name in enum_attributes:
                    raise ValueError((
                        "Enum attribute name included both in {} "
                        "and {}: {}").format(
                        cls.ENUM_ATTRIBUTES_ATTR_NAME,
                        cls.ENUM_EXCLUDED_ATTR_NAME,
                        name,
                    ))
                continue
            value = attrs[name]
            if cls.is_enum_attribute(name, value):
                enum_attrs[name] = value
            elif enum_attributes:
                # the value was given explicitly - raise error
                raise ValueError(
                    "Enum constant has invalid name or value, {}: {}".format(
                        name, value)
                )

        if not enum_attributes and cls.should_sort_enum_values(attrs):
            enum_attrs = cls.sort_enum_attrs(enum_attrs, attrs)

        return enum_attrs

    @classmethod
    def get_inherited(cls, bases, is_ordered):
        enum_bases = filter(
            lambda x: getattr(x, '__metaclass__', None) == cls, bases)
        if len(enum_bases) == 0:
            return {}

        dict_class = cls.get_dict_class(is_ordered)
        inherited = {}
        for base in enum_bases:
            for attrname in cls.EXTRA_FIELDS:
                value = getattr(base, attrname)
                if isinstance(value, dict):
                    inherited.setdefault(
                        attrname, dict_class()).update(value)
                elif isinstance(value, list):
                    inherited.setdefault(attrname, []).extend(value)

        return inherited

    @classmethod
    def get_dict_class(cls, is_ordered):
        return OrderedDict if is_ordered else dict

    @classmethod
    def create_values(cls, enum_attrs, attrs, is_ordered, inherited=[]):
        return inherited + enum_attrs.values()

    @classmethod
    def create_names(cls, enum_attrs, attrs, is_ordered, inherited=None):
        result = inherited or cls.get_dict_class(is_ordered)()
        result.update([(v, k) for k, v in enum_attrs.items()])
        return result

    @classmethod
    def create_value_for_name(cls, enum_attrs, attrs, is_ordered, inherited=None):
        result = inherited or cls.get_dict_class(is_ordered)()
        result.update(enum_attrs)
        return result

    @classmethod
    def create_display_names(cls, enum_attrs, attrs, is_ordered, inherited=None):
        result = inherited or cls.get_dict_class(is_ordered)()
        result.update(
            [(v, cls.make_display_name(k)) for k, v in enum_attrs.items()]
        )
        return result

    @classmethod
    def create_choices(cls, enum_attrs, attrs, is_ordered, inherited=[]):
        return inherited + [(v, k) for k, v in enum_attrs.items()]

    @classmethod
    def create_display_choices(cls, enum_attrs, attrs, is_ordered, inherited=[]):
        dn = attrs['display_names']
        return inherited + [(v, dn[v]) for k, v in enum_attrs.items()]

    @classmethod
    def create_as_json(cls, enum_attrs, attrs, is_ordered, inherited=None):
        result = inherited or cls.get_dict_class(is_ordered)()
        result.update(
            [(name, attrs[name]) for name in cls.EXTRA_FIELDS
                if name != 'as_json'] +
            attrs['value_for_name'].items()
        )
        return result

    @classmethod
    def validate_value_uniqueness(cls, attrs):
        counter = Counter(attrs['values'])
        duplicate = [x[0] for x in counter.most_common() if x[1] > 1]
        if len(duplicate) > 0:
            def is_enum_label(attr, val):
                return val in duplicate and attr in attrs['value_for_name']
            names = [n for n, v in attrs.items() if is_enum_label(n, v)]
            raise ValueError(
                "Following names share the same value: {}".format(
                    ", ".join(sorted(names))))

    @classmethod
    def process_enum_fields(cls, class_name, bases, attrs):
        is_ordered = cls.should_preserve_value_order(bases, attrs)
        attrs[cls.PRIVATE_IS_ORDERED_ATTR_NAME] = is_ordered

        enum_attrs = cls.get_enum_attrs(bases, attrs, is_ordered)
        inherited = cls.get_inherited(bases, is_ordered)

        # copy inherited constants
        for value, name in inherited.get('names', {}).items():
            # checking if we are not overriding by mistake
            if name in enum_attrs:
                raise ValueError(
                    ("Attempting to overwrite an existing enum value: "
                        "{name}").format(name=name))
            attrs[name] = value

        # create enum attributes
        for attrname in cls.EXTRA_FIELDS:
            handler = getattr(cls, 'create_{}'.format(attrname))
            args = [enum_attrs, attrs, is_ordered]
            inherited.get(attrname) and args.append(inherited.get(attrname))
            old = attrs.get(attrname)
            attrs[attrname] = handler(*args)
            if old is not None and isinstance(old, dict):
                attrs[attrname].update(old)

        cls.validate_value_uniqueness(attrs)

        return class_name, bases, attrs

    def __new__(cls, class_name, bases, attrs):
        class_name, bases, attrs = cls.process_enum_fields(
            class_name, bases, attrs)
        return type.__new__(cls, class_name, bases, attrs)
