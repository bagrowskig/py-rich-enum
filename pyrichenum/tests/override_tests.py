# -*- coding: utf8 -*-

import unittest

from unittest import TestCase
from pyrichenum.enum import EnumMetaclass


class TestEnum(object):

    __metaclass__ = EnumMetaclass

    VAL1 = 1
    VAL2 = 2
    VAL3 = 3

    display_names = {
        VAL1: 'Custom val1 display',
    }


class EnumMetaclassOverrideTestCase(TestCase):

    _test_enum = TestEnum

    def test_display_names(self):
        self.assertEqual(
            self._test_enum.display_names[1],
            'Custom val1 display'
        )
        self.assertEqual(self._test_enum.display_names[2], 'Val2')

    def test_display_choices(self):
        self.assertItemsEqual(
            self._test_enum.display_choices,
            [(1, 'Custom val1 display'), (2, 'Val2'), (3, 'Val3')]
        )


if __name__ == '__main__':
    unittest.main()
