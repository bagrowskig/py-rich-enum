import unittest

from .enum import *
from .ordered import *
from .inheritance import *
from .multiinheritance import *
from .exclusion import *
from .override_tests import *
from .sorted import *


if __name__ == '__main__':
    unittest.main()
