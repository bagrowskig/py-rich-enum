# -*- coding: utf8 -*-

from __future__ import unicode_literals

import unittest
from unittest import TestCase

from pyrichenum.enum import EnumMetaclass
from .enum import EnumMetaclassTestCase


class InheritanceTestBaseEnum(object):

    __metaclass__ = EnumMetaclass

    VAL1 = 1
    VAL2 = 'a'


class InheritanceTestEnum(InheritanceTestBaseEnum):

    __metaclass__ = EnumMetaclass

    VAL3 = u'ó'
    VAL4 = 0.3


class EnumMetaclassInheritanceTestCase(EnumMetaclassTestCase):

    _test_enum = InheritanceTestEnum


class EnumMetaclassInheritanceErrorsTestCase(TestCase):

    def test_inheritance__duplicate_value_error(self):
        try:
            class InheritanceDuplicateFailTestEnum(InheritanceTestBaseEnum):

                __metaclass__ = EnumMetaclass

                VAL3 = 1

        except ValueError, e:
            self.assertEqual(
                "Following names share the same value: {}".format(
                    ", ".join(sorted(["VAL3", "VAL1"]))),
                str(e)
            )
            raised = True
        else:
            raised = False

        self.assertTrue(raised)

    def test_inheritance__duplicate_name_error(self):
        try:
            class InheritanceDuplicateFailTestEnum(InheritanceTestBaseEnum):

                __metaclass__ = EnumMetaclass

                VAL1 = 1

        except ValueError, e:
            self.assertEqual(
                "Attempting to overwrite an existing enum value: VAL1",
                str(e)
            )
            raised = True
        else:
            raised = False

        self.assertTrue(raised)


if __name__ == '__main__':
    unittest.main()
