# -*- coding: utf8 -*-

import unittest

from unittest import TestCase
from collections import OrderedDict
from pyrichenum.enum import EnumMetaclass


class OrderedTestEnum(object):

    __metaclass__ = EnumMetaclass

    VAL3 = u'ó'
    VAL1 = 1
    VAL2 = 'a'
    VAL4 = 0.3

    # Required only to preserve the constants order
    enum_attributes = [
        'VAL1',
        'VAL2',
        'VAL3',
        'VAL4',
    ]


class OrderedEnumMetaclassTestCase(TestCase):

    def test_properties(self):
        self.assertEqual(OrderedTestEnum.VAL1, 1)
        self.assertEqual(OrderedTestEnum.VAL2, 'a')
        self.assertEqual(OrderedTestEnum.VAL3, u'ó')
        self.assertEqual(OrderedTestEnum.VAL4, 0.3)

    def test_values(self):
        self.assertEqual(OrderedTestEnum.values, [1, 'a', u'ó', 0.3])
        self.assertNotEqual(OrderedTestEnum.values, ['a', u'ó', 0.3, 1])

    def test_names(self):
        self.assertEqual(OrderedTestEnum.names,  OrderedDict(
            [(1, 'VAL1'), ('a', 'VAL2'), (u'ó', 'VAL3'), (0.3, 'VAL4')]
        ))
        self.assertNotEqual(OrderedTestEnum.names,  OrderedDict(
            [(0.3, 'VAL4'), (1, 'VAL1'), ('a', 'VAL2'), (u'ó', 'VAL3')]
        ))

    def test_display_names(self):
        self.assertEqual(OrderedTestEnum.display_names, OrderedDict(
            [(1, 'Val1'), ('a', 'Val2'), (u'ó', 'Val3'), (0.3, 'Val4')]
        ))
        self.assertNotEqual(OrderedTestEnum.display_names, OrderedDict(
            [(0.3, 'Val4'), (1, 'Val1'), ('a', 'Val2'), (u'ó', 'Val3')]
        ))

    def test_value_for_name(self):
        self.assertEqual(OrderedTestEnum.value_for_name, OrderedDict(
            [('VAL1', 1), ('VAL2', 'a'), ('VAL3', u'ó'), ('VAL4', 0.3)]
        ))
        self.assertNotEqual(OrderedTestEnum.value_for_name, OrderedDict(
            [('VAL4', 0.3), ('VAL1', 1), ('VAL2', 'a'), ('VAL3', u'ó')]
        ))

    def test_choices(self):
        self.assertEqual(OrderedTestEnum.choices, [
            (1, 'VAL1'), ('a', 'VAL2'), (u'ó', 'VAL3'), (0.3, 'VAL4')
        ])

    def test_display_choices(self):
        self.assertEqual(OrderedTestEnum.display_choices, [
            (1, 'Val1'), ('a', 'Val2'), (u'ó', 'Val3'), (0.3, 'Val4')
        ])


if __name__ == '__main__':
    unittest.main()
