# -*- coding: utf8 -*-

import unittest

from unittest import TestCase
from pyrichenum.enum import EnumMetaclass


class TestEnum(object):

    __metaclass__ = EnumMetaclass

    VAL1 = 1
    VAL2 = 'a'
    VAL3 = u'ó'
    VAL4 = 0.3


class EnumMetaclassTestCase(TestCase):

    _test_enum = TestEnum

    def test_properties(self):
        self.assertEqual(self._test_enum.VAL1, 1)
        self.assertEqual(self._test_enum.VAL2, 'a')
        self.assertEqual(self._test_enum.VAL3, u'ó')
        self.assertEqual(self._test_enum.VAL4, 0.3)

    def test_values(self):
        self.assertItemsEqual(self._test_enum.values, [1, 'a', u'ó', 0.3])

    def test_names(self):
        self.assertDictEqual(self._test_enum.names,  {
            'a': 'VAL2', 1: 'VAL1', u'ó': 'VAL3', 0.3: 'VAL4'
        })

    def test_display_names(self):
        self.assertDictEqual(self._test_enum.display_names, {
            'a': 'Val2', 1: 'Val1', u'ó': 'Val3', 0.3: 'Val4'
        })

    def test_value_for_name(self):
        self.assertDictEqual(self._test_enum.value_for_name, {
            'VAL3': u'ó', 'VAL2': 'a', 'VAL1': 1, 'VAL4': 0.3
        })

    def test_choices(self):
        self.assertItemsEqual(self._test_enum.choices, [
            (u'ó', 'VAL3'), ('a', 'VAL2'), (1, 'VAL1'), (0.3, 'VAL4')
        ])

    def test_display_choices(self):
        self.assertItemsEqual(self._test_enum.display_choices, [
            (u'ó', 'Val3'), ('a', 'Val2'), (1, 'Val1'), (0.3, 'Val4')
        ])

    def text_as_json(self):
        self.assertDictEqual(self._test_enum.as_json, {
            'display_names': {
                'a': 'Val2', 1: 'Val1', u'ó': 'Val3', 0.3: 'Val4'
            },
            'value_for_name': {
                'VAL3': u'ó', 'VAL2': 'a', 'VAL1': 1, 'VAL4': 0.3
            },
            'choices': (
                (u'ó', 'VAL3'), ('a', 'VAL2'), (1, 'VAL1'), (0.3, 'VAL4')
            ),
            'values': ['a', 1, u'ó', 0.3],
            'names': {
                'a': 'VAL2', 1: 'VAL1', u'ó': 'VAL3', 0.3: 'VAL4'
            },
            'VAL3': u'ó',
            'VAL2': 'a',
            'VAL1': 1,
            'VAL4': 0.3,
            'display_choices': (
                (u'ó', 'Val3'), ('a', 'Val2'), (1, 'Val1'), (0.3, 'Val4')
            ),
        })


class EnumMetaclassValueErrorTestCase(TestCase):

    def test_invalid_enum_value_error(self):
        try:
            class InheritanceDuplicateFailTestEnum(object):

                __metaclass__ = EnumMetaclass

                VAL1 = 1
                val2 = 'asd'

                enum_attributes = ['val2', ]

        except ValueError, e:
            raised = True
            self.assertEqual(
                "Enum constant has invalid name or value, val2: asd",
                str(e)
            )
        else:
            raised = False

        self.assertTrue(raised)

        try:
            class InheritanceDuplicateFailTwoTestEnum(object):

                __metaclass__ = EnumMetaclass

                VAL1 = 1
                VAL2 = lambda: 1

                enum_attributes = ['VAL2', ]

        except ValueError, e:
            raised = True
            self.assertTrue(
                str(e).startswith(
                    ("Enum constant has invalid name or value, VAL2: "
                        "<function <lambda> at")))
        else:
            raised = False

        self.assertTrue(raised)

    def test_duplicate_value_error(self):
        try:
            class InheritanceDuplicateFailTestEnum(object):

                __metaclass__ = EnumMetaclass

                VAL1 = 1
                VAL2 = 1

        except ValueError, e:
            self.assertEqual(
                "Following names share the same value: {}".format(
                    ", ".join(sorted(["VAL2", "VAL1"]))),
                str(e)
            )
            raised = True
        else:
            raised = False

        self.assertTrue(raised)

if __name__ == '__main__':
    unittest.main()
