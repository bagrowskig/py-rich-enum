# -*- coding: utf8 -*-

from unittest import TestCase
from pyrichenum.enum import EnumMetaclass
from .enum import EnumMetaclassTestCase


class TestExcludeEnum(object):

    __metaclass__ = EnumMetaclass

    VAL1 = 1
    VAL2 = 'a'
    VAL3 = u'ó'
    VAL4 = 0.3
    VAL5 = 'asd'

    enum_excluded = ['VAL5']


class ExcludeTestCase(EnumMetaclassTestCase):

    _test_enum = TestExcludeEnum


class TestIncludeEnum(object):

    __metaclass__ = EnumMetaclass

    VAL1 = 1
    VAL2 = 'a'
    VAL3 = u'ó'
    VAL4 = 0.3
    VAL5 = 'asd'
    VAL6 = 'asd1'
    VAL7 = 'asd2'

    enum_attributes = ['VAL1', 'VAL2', 'VAL3', 'VAL4']


class IncludeTestCase(EnumMetaclassTestCase):

    _test_enum = TestIncludeEnum


class ExcludeIncludeTestCase(TestCase):
    """When a field is added to """

    def test_include_exclude_error(self):
        try:
            class TestIncludeExcludeEnum(object):
                __metaclass__ = EnumMetaclass
                VAL1 = 1
                VAL2 = 'a'
                enum_excluded = ['VAL1', 'VAL2']
                enum_attributes = ['VAL1']
        except ValueError, e:
            self.assertEqual(
                ("Enum attribute name included both in enum_attributes "
                    "and enum_excluded: {}").format('VAL1'),
                str(e)
            )
            raised = True
        else:
            raised = False

        self.assertTrue(raised)
