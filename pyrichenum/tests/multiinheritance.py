# -*- coding: utf8 -*-

from __future__ import unicode_literals

import unittest

from pyrichenum.enum import EnumMetaclass
from .enum import EnumMetaclassTestCase


class BaseEnum(object):

    __metaclass__ = EnumMetaclass

    VAL1 = 1
    VAL2 = 'a'


class ThreeValueEnum(BaseEnum):

    __metaclass__ = EnumMetaclass

    VAL3 = u'ó'


class FourValueEnum(ThreeValueEnum):

    __metaclass__ = EnumMetaclass

    VAL4 = 0.3


class EnumMetaclassMultiInheritanceTestCase(EnumMetaclassTestCase):
    """Inheriting gradually."""
    __metaclass__ = EnumMetaclass

    _test_enum = FourValueEnum


class VAL1Enum(object):

    __metaclass__ = EnumMetaclass

    VAL1 = 1


class VAL2Enum(object):

    __metaclass__ = EnumMetaclass

    VAL2 = 'a'


class VAL3Enum(object):

    __metaclass__ = EnumMetaclass

    VAL3 = u'ó'


class FourValueEnum2(VAL1Enum, VAL2Enum, VAL3Enum):

    __metaclass__ = EnumMetaclass

    VAL4 = 0.3


class EnumMetaclassMultiInheritanceTwoTestCase(EnumMetaclassTestCase):
    """Inheriting all at once."""

    __metaclass__ = EnumMetaclass

    _test_enum = FourValueEnum2


if __name__ == '__main__':
    unittest.main()
