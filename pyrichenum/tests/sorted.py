# -*- coding: utf8 -*-

import unittest

from unittest import TestCase
from collections import OrderedDict
from pyrichenum.enum import EnumMetaclass


class UnsortedTestEnum(object):
    """Enum applying sorting by value (default)."""

    __metaclass__ = EnumMetaclass

    VAL1 = 'c'
    VAL2 = 'b'
    VAL3 = 'a'
    VAL4 = 'd'
    VAL5 = 'f'
    VAL6 = 'e'
    VAL7 = 'g'
    VAL8 = 'h'

    sort_enum_values = False


class UnsortedEnumMetaclassTestCase(TestCase):
    """Checks if the fields won't get alphabetically sorted if
    sort_enum_values = False. It can randomly produce a fail when the values
    happen to order themselves alphabetically by value, but with 8 values
    there is a very slim chance(?).

    """

    _test_enum = UnsortedTestEnum

    def test_properties(self):
        self.assertEqual(self._test_enum.VAL1, 'c')
        self.assertEqual(self._test_enum.VAL2, 'b')
        self.assertEqual(self._test_enum.VAL3, 'a')
        self.assertEqual(self._test_enum.VAL4, 'd')
        self.assertEqual(self._test_enum.VAL5, 'f')
        self.assertEqual(self._test_enum.VAL6, 'e')
        self.assertEqual(self._test_enum.VAL7, 'g')
        self.assertEqual(self._test_enum.VAL8, 'h')

    def test_values(self):
        self.assertNotEqual(
            self._test_enum.values, ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
        )

    def test_names(self):
        self.assertNotEqual(self._test_enum.names,  OrderedDict(
            [
                ('a', 'VAL3'), ('b', 'VAL2'), ('c', 'VAL1'), ('d', 'VAL4'),
                ('e', 'VAL5'), ('f', 'VAL5'), ('g', 'VAL7'), ('h', 'VAL8'),
            ]
        ))


class SortedTestEnum(object):
    """Enum applying sorting by value (default)."""

    __metaclass__ = EnumMetaclass

    VAL4 = 'd'
    VAL1 = 'c'
    VAL2 = 'b'
    VAL3 = 'a'

    # sort_enum_values = True  # the default


class ValueSortedEnumMetaclassTestCase(TestCase):

    _test_enum = SortedTestEnum

    def _orderer(self, items):
        return items

    def test_properties(self):
        self.assertEqual(self._test_enum.VAL1, 'c')
        self.assertEqual(self._test_enum.VAL2, 'b')
        self.assertEqual(self._test_enum.VAL3, 'a')
        self.assertEqual(self._test_enum.VAL4, 'd')

    def test_values(self):
        self.assertEqual(
            self._test_enum.values,
            self._orderer(['a', 'b', 'c', 'd'])
        )
        self.assertNotEqual(self._test_enum.values, ['d', 'c', 'b', 'a'])

    def test_names(self):
        self.assertEqual(self._test_enum.names,  OrderedDict(
            self._orderer([
                ('a', 'VAL3'), ('b', 'VAL2'), ('c', 'VAL1'), ('d', 'VAL4')
            ])
        ))
        self.assertNotEqual(self._test_enum.names,  OrderedDict(
            [('d', 'VAL4'), ('c', 'VAL1'), ('b', 'VAL2'), ('a', 'VAL3')]
        ))


class KeySortedTestEnum(object):
    """Enum applying sorting by key name instead of default value."""

    __metaclass__ = EnumMetaclass

    VAL4 = 'd'
    VAL1 = 'c'
    VAL2 = 'b'
    VAL3 = 'a'

    # sort_enum_values = True  # the default
    enum_values_sorted_kwargs = {
        'key': lambda x: x[0],
    }


class KeySortedEnumMetaclassTestCase(TestCase):

    _test_enum = KeySortedTestEnum

    def _orderer(self, items):
        return items

    def test_properties(self):
        self.assertEqual(self._test_enum.VAL1, 'c')
        self.assertEqual(self._test_enum.VAL2, 'b')
        self.assertEqual(self._test_enum.VAL3, 'a')
        self.assertEqual(self._test_enum.VAL4, 'd')

    def test_values(self):
        self.assertEqual(
            self._test_enum.values,
            self._orderer(['c', 'b', 'a', 'd'])
        )
        self.assertNotEqual(self._test_enum.values, ['d', 'c', 'b', 'a'])

    def test_names(self):
        self.assertEqual(self._test_enum.names,  OrderedDict(
            self._orderer([
                ('c', 'VAL1'), ('b', 'VAL2'), ('a', 'VAL3'), ('d', 'VAL4')
            ])
        ))
        self.assertNotEqual(self._test_enum.names,  OrderedDict(
            [('d', 'VAL4'), ('c', 'VAL1'), ('b', 'VAL2'), ('a', 'VAL3')]
        ))


class ReverseKeySortedTestEnum(object):
    """Enum applying sorting in reverse by key name instead of default value."""

    __metaclass__ = EnumMetaclass

    VAL4 = 'd'
    VAL1 = 'c'
    VAL2 = 'b'
    VAL3 = 'a'

    # sort_enum_values = True  # the default
    enum_values_sorted_kwargs = {
        'key': lambda x: x[0],
        'reverse': True,
    }


class ReverseKeySortedEnumMetaclassTestCase(KeySortedEnumMetaclassTestCase):

    _test_enum = ReverseKeySortedTestEnum

    def _orderer(self, items):
        items.reverse()
        return items


if __name__ == '__main__':
    unittest.main()
